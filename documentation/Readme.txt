In diesem Ordner sind 
- die Dokumentation (Dokumentation.pdf), 
- die APK (Billreader.pdf), 
- der Quellcode (Ordner OCRReader) und 
- ein Testdokument, an dem die Funktionalit�t getestet werden kann (Test.pdf)
zu finden.

F�r die Installation kann ein Android-Ger�t �ber USB angeschlossen werden und die APK mittels 
'adb install BillReader.apk' installiert werden. F�r die korrekte Ausf�hrung ist es ratsam 
ein Smartphone mit einer hochaufl�senden Kamera zu verwenden, damit die Erkennung der Elemente
gew�hrleistet ist. Es kann sein das auf Grund eines Skalierungsfehlers der freigestellte Bereich
bei der Erkennung gr��er sein muss, dies erkennt der Nutzer daran, wenn alle relevanten Elemente 
als Text in der Vorschau angezeigt werden.

Zudem ist der Quellcode Open-Source und unter 'https://bitbucket.org/richardboehm/bill-reader/' 
ersichtlich.

Kontakt:
Richard B�hm
ribo0001@stud.hs-kl.de