package me.richardboehm.ocrreader.ui.details

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import me.richardboehm.ocrreader.R
import me.richardboehm.ocrreader.models.Category

/**
 * Dialog to edit existing @[me.richardboehm.ocrreader.models.ReceiptItem]s in the
 * @[DetailListActivity].
 */
class ItemDialogFragment : DialogFragment() {

    private var position: Int = 0
    private var name: String? = null
    private var category: Int = 0

    private var listener: DialogListener? = null

    interface DialogListener {
        fun onDialogPositiveClick(position: Int, name: String, cost: Float, category: Int)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as DialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException(String.format("%s must implement DialogListener", context.toString()))
        }

    }

    /**
     * Creates a dialog with the options to change the name, cost and category of an item.
     */
    @SuppressLint("DefaultLocale")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val args = arguments
        val builder = AlertDialog.Builder(activity)
        // Get the layout inflater
        val inflater = activity.layoutInflater


        this.position = args.getInt("position")
        this.name = args.getString("name")
        val cost = args.getFloat("cost")
        this.category = args.getInt("category")

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        val view = inflater.inflate(R.layout.dialog_update_item, null)
        val editTextName = view.findViewById<EditText>(R.id.et_item_name)
        val editTextCost = view.findViewById<EditText>(R.id.et_item_cost)
        editTextName.setText(this.name)
        editTextCost.setText(cost.toString())

        val spinnerCategory = view.findViewById<Spinner>(R.id.spinner_category)
        val adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, Category.categories)
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Apply the adapter to the spinner
        spinnerCategory.adapter = adapter
        spinnerCategory.post { spinnerCategory.setSelection(category) }


        builder.setView(view)
                // Add action buttons
                .setPositiveButton(R.string.confirm_update_item) { dialog, id ->
                    // Return callback

                    listener!!.onDialogPositiveClick(position, editTextName.text.toString(), java.lang.Float.parseFloat(editTextCost.text.toString()), spinnerCategory.selectedItemPosition)
                }
                .setNegativeButton(R.string.cancel_update_item) { dialog, id -> this@ItemDialogFragment.dialog.cancel() }
        return builder.create()
    }
}
