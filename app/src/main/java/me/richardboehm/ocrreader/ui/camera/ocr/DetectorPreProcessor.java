package me.richardboehm.ocrreader.ui.camera.ocr;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.ByteArrayOutputStream;

import me.richardboehm.ocrreader.ui.camera.OcrCaptureActivity;

/**
 * {@link DetectorPreProcessor} is used to limit the detection area before sending it to the
 * {@link OcrDetectorProcessor}.
 * @author Richard Böhm
 * @version 1.0
 */
public class DetectorPreProcessor extends Detector<TextBlock> {


    private static final String TAG = DetectorPreProcessor.class.getName();
    float displayWidth, displayHeight;
    private TextRecognizer delegate;
    private OcrCaptureActivity ocr;
    private View limitFrame;
    private DisplayMetrics displayMetrics;

    /**
     * Constructor.
     *
     * @param delegate       Delegate for the data to be sent to after the task is done.
     * @param limitFrame     The limiting frame.
     * @param ocr            The {@link OcrCaptureActivity} to later set the correct offset, which we need to
     *                       apply to the {@link OcrGraphic}.
     * @param displayMetrics The display size.
     */
    public DetectorPreProcessor(TextRecognizer delegate, View limitFrame, OcrCaptureActivity ocr, DisplayMetrics displayMetrics) {
        this.delegate = delegate;
        this.limitFrame = limitFrame;
        this.displayMetrics = displayMetrics;
        displayWidth = displayMetrics.widthPixels;
        displayHeight = displayMetrics.heightPixels;
        this.ocr = ocr;
    }

    /**
     * Calculates the proportions between the given view and the frame. Used to calculate the
     * relative size of the crop area and the actual screen size.
     * @return Returns a float array with the relative x- and y-proportions.
     */
    public float[] recalculateRelation(View view, Frame.Metadata metadata) {
        float imageWidth = metadata.getWidth();
        float imageHeight = metadata.getHeight();
        return new float[]{imageWidth / displayWidth, imageHeight / displayHeight};
    }

    /**
     * Calculate the bounding area to which limits the detection area. Both the camera frame
     * and the display metrics return different sizes, as the camera preview does not support
     * high resolution previews. This makes it necessary to calculate the relative sizes in the
     * x- and y-coordinates (width and height), to scale the bounding area. This new
     * cropping area also affects the text rendering on top of the preview. The text has it's
     * relative position from the processed frame, thus the coordinates do not match the original
     * frame size, as it has been rescaled. This results in an incorrect positioning of the text
     * elements (e.g top-right corner). See @{@link OcrGraphic}.
     * @param frame The input frame received from the camera.
     * @return Returns a compressed and resized version of the frame according to the users area
     * selection.
     */
    @SuppressLint("NewApi")
    public SparseArray<TextBlock> detect(Frame frame) {
        // Calculate the relative proportions
        float[] offset = recalculateRelation((View) limitFrame.getParent(), frame.getMetadata());
        int width = frame.getMetadata().getWidth();
        int height = frame.getMetadata().getHeight();
        // int orientation = ocr.getResources().getConfiguration().orientation;
        int frameWidth = limitFrame.getWidth();//orientation == Configuration.ORIENTATION_LANDSCAPE ? limitFrame.getWidth() : limitFrame.getHeight();
        int frameHeight = limitFrame.getHeight(); //orientation == Configuration.ORIENTATION_LANDSCAPE ? limitFrame.getHeight() : limitFrame.getWidth();

        // Calculate correct bounding area. For some reason the width and height are mixed up.
        float right = ((height / 2.0f) + (frameHeight / 2.0f) * offset[1]);
        ; //offset[0];
        right = Math.min(right, height - 8);
        float left = ((height / 2.0f) - (frameHeight / 2.0f) * offset[1]);  //(((displayWidth / 2.0f) - (frameWidth / 2.0f))) * offset[0];
        left = Math.max(left, 8);
        float bottom = ((width / 2.0f) + (frameWidth / 2.0f) * offset[0]); //(((displayHeight / 2.0f) + (frameHeight / 2.0f))) * offset[1];
        bottom = Math.min(bottom, height - 8);
        float top = ((width / 2.0f) - (frameWidth / 2.0f) * offset[0]); //(((displayHeight / 2.0f) - (frameHeight / 2.0f))) * offset[1];
        top = Math.max(top, 8);
        ocr.setOffest(new float[]{left, top});

        Log.d(TAG, String.format("TOP: %s - RIGHT: %s - BOTTOM: %s - LEFT: %s", top, right, bottom, left));
        /**
         * Crop the image using @android.graphics.YuvImage. By turning the image into a YuvImage
         * we can compress and resize the image. The returned {@link ByteArrayOutputStream} can be
         * decoded back into a bitmap, which we can use to create a frame
         */
        YuvImage yuvImage = new YuvImage(frame.getGrayscaleImageData().array(), ImageFormat.NV21, width, height, null);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect((int) left, (int) top, (int) right, (int) bottom), 100, byteArrayOutputStream);
        byte[] jpegArray = byteArrayOutputStream.toByteArray();
        Bitmap bitmap = BitmapFactory.decodeByteArray(jpegArray, 0, jpegArray.length);

        // Build the cropped frame and return it to the delegate.
        Frame croppedFrame =
                new Frame.Builder()
                        .setBitmap(bitmap)
                        .setRotation(frame.getMetadata().getRotation())
                        .build();
        return delegate.detect(croppedFrame);
    }

    public boolean isOperational() {
        return delegate.isOperational();
    }

    public boolean setFocus(int id) {
        return delegate.setFocus(id);
    }
}
