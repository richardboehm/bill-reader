package me.richardboehm.ocrreader.ui.camera.ocr

import android.annotation.SuppressLint
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

import me.richardboehm.ocrreader.R

/**
 * [CropArea] is a resizable view, which is supposed to limit the detection area of the
 * [OcrDetectorProcessor].
 * @author Richard Böhm
 * @version 1.0
 */
class CropArea
/**
 * Constructor.
 *
 * @param context The context of the activity.
 * @param attrs   The AttributeSet.
 */
(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    internal var parent: View? = null
    internal var centerX: Int = 0
    internal var centerY: Int = 0

    @SuppressLint("ClickableViewAccessibility")
    override fun onFinishInflate() {
        super.onFinishInflate()
        parent = findViewById(R.id.crop_limit)
    }

    /**
     * Calculates the distance from the center. Takes the x and y values of a position as input and
     * returns a float array containing the horizontal and vertical distance from the middle point.
     * @param x X-coordinate of the position.
     * @param y Y-coordinate of the position.
     * @return Returns the horizontal and vertical distance as a float array.
     */
    private fun getDistance(x: Float, y: Float): FloatArray {
        return floatArrayOf(Math.abs(x - centerX), Math.abs(y - centerY))
    }

    /**
     * The onTouchEvent methode gets called when the user clicks on the view. We calculate the
     * distance between the interaction point and the center of the screen and set the sides of the
     * view accordingly.
     * @param event
     * @return
     */
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.rawX
        val y = event.rawY
        val action = event.action
        when (action) {
            MotionEvent.ACTION_DOWN -> {
                centerX = (parent!!.left + parent!!.width / 2.0f).toInt()
                centerY = (parent!!.top + parent!!.height / 2.0f).toInt()
            }
            MotionEvent.ACTION_MOVE -> {
                val distance = getDistance(x, y)
                parent!!.left = (centerX - distance[0]).toInt()
                parent!!.right = (centerX + distance[0]).toInt()


                parent!!.top = (centerY - distance[1]).toInt()
                parent!!.bottom = (centerY + distance[1]).toInt()
            }
            MotionEvent.ACTION_UP -> {
            }
            MotionEvent.ACTION_CANCEL -> {
            }

            MotionEvent.ACTION_POINTER_UP -> {
            }
        }
        return true
    }

    companion object {

        private val TAG = CropArea::class.java.name
    }
}