package me.richardboehm.ocrreader.ui.intro

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

import me.richardboehm.ocrreader.R

/**
 * Basic Adapter which limits the page count to 3 and sets the color to the first pages color defined
 * in R.color
 */
class IntroAdapter(private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return IntroFragment.newInstance(context.resources.getColor(R.color.intro01), position)
    }

    override fun getCount(): Int {
        return 3
    }
}
