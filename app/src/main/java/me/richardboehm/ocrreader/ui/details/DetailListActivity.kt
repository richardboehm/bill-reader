package me.richardboehm.ocrreader.ui.details

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatCallback
import android.support.v7.app.AppCompatDelegate
import android.support.v7.view.ActionMode
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import me.richardboehm.ocrreader.R
import me.richardboehm.ocrreader.util.DatabaseHelper
import java.text.NumberFormat

/**
 * When the user clicks on an item in @[me.richardboehm.ocrreader.ui.overview.MainActivity] he opens
 * the @[DetailListActivity]. This activity
 */
class DetailListActivity : Activity(), AppCompatCallback, DetailListFragment.OnTotalPass, ItemDialogFragment.DialogListener {
    private var receiptId: Long = 0
    private var receiptDate: String? = null

    /**
     * OnCreate we fetch the receiptId and receiptDate from the bundled extras. The id is used to
     * pull the data from the database, the date is only used to display the "name" of the receipt
     * on the toolbar, which is set using a delegate.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        receiptId = intent.extras!!.getLong("receiptId")
        receiptDate = intent.extras!!.getString("receiptDate")
        val delegate = AppCompatDelegate.create(this, this)
        delegate.onCreate(savedInstanceState)
        delegate.setContentView(R.layout.activity_detail_list)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = receiptDate
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        delegate.setSupportActionBar(toolbar)
    }

    /**
     * If the user clicks on the positive dialog button then the list adapter is updated and we
     * display the new total cost.
     */
    override fun onDialogPositiveClick(position: Int, name: String, cost: Float, category: Int) {
        val detailListFragment = fragmentManager.findFragmentById(R.id.fragment_list_item) as DetailListFragment
        detailListFragment.updateItem(position, name, cost, category)
        updateAdapter()
        detailListFragment.total
    }

    /**
     * Here we update the list adapter after the data set changed.
     */
    private fun updateAdapter() {
        val detailListFragment = fragmentManager.findFragmentById(R.id.fragment_list_item) as DetailListFragment
        detailListFragment.adapter.notifyDataSetChanged()
        detailListFragment.passTotal()
    }

    /**
     * Inflates the menu.
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_detail, menu)
        return true
    }

    /**
     * After the list is updated we need to also update the corresponding receipt on the database.
     * This is a callback method which is called by the @[DetailListFragment] when an item is updated.
     * @see @[DetailListFragment]
     */
    override fun onTotalPass(data: Float) {
        val format = NumberFormat.getCurrencyInstance()
        val tvTotal = findViewById<TextView>(R.id.tv_total)
        tvTotal.text = format.format(data.toDouble())
        val database = DatabaseHelper(this)
        database.updateReceipt(receiptId, data)
    }

    /**
     * If the user interacts with the toolbar we react to that here. Right now the user can only
     * delete the receipt.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_delete -> {
                val database = DatabaseHelper(this)
                val deleted = database.deleteReceipt(receiptId)
                Log.d(TAG, String.format("Deleted %d item(s)", deleted))
                finish()
                return true
            }
            else -> {
                finish()
                return super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onSupportActionModeStarted(mode: ActionMode) {}

    override fun onSupportActionModeFinished(mode: ActionMode) {}

    override fun onWindowStartingSupportActionMode(callback: ActionMode.Callback): ActionMode? {
        return null
    }

    companion object {

        private val TAG = DetailListActivity::class.java.name
    }
}
