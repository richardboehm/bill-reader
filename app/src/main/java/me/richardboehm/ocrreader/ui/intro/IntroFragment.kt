package me.richardboehm.ocrreader.ui.intro

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import me.richardboehm.ocrreader.R

/**
 * Fragment to be displayed in the @[IntroductionActivity].
 */
class IntroFragment : Fragment() {

    private var backgroundColor: Int = 0
    private var page: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!arguments!!.containsKey(BACKGROUND_COLOR))
            throw RuntimeException("Fragment must contain a \"$BACKGROUND_COLOR\" argument!")
        backgroundColor = arguments!!.getInt(BACKGROUND_COLOR)

        if (!arguments!!.containsKey(PAGE))
            throw RuntimeException("Fragment must contain a \"$PAGE\" argument!")
        page = arguments!!.getInt(PAGE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        // Select a layout based on the current page
        val layoutResId: Int
        when (page) {
            0 -> layoutResId = R.layout.fragment_intro_1
            1 -> layoutResId = R.layout.fragment_intro_2
            else -> layoutResId = R.layout.fragment_intro_3
        }

        // Inflate the layout resource file
        val view = activity!!.layoutInflater.inflate(layoutResId, container, false)

        // Set the current page index as the View's tag (useful in the PageTransformer)
        view.tag = page

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Set the background color of the root view to the color specified in newInstance()
        val background = view.findViewById<View>(R.id.intro_background)
        background.setBackgroundColor(backgroundColor)
    }

    companion object {

        private val BACKGROUND_COLOR = "backgroundColor"
        private val PAGE = "page"

        fun newInstance(backgroundColor: Int, page: Int): IntroFragment {
            val frag = IntroFragment()
            val b = Bundle()
            b.putInt(BACKGROUND_COLOR, backgroundColor)
            b.putInt(PAGE, page)
            frag.arguments = b
            return frag
        }
    }

}
