package me.richardboehm.ocrreader.ui.intro

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.FragmentActivity
import android.support.v4.view.ViewPager
import android.view.View

import me.richardboehm.ocrreader.R
import me.richardboehm.ocrreader.ui.overview.MainActivity

/**
 * This activity gets called after the user opens the application for the first time. It implements
 * @[FragmentActivity] and basically has a ViewPager to display different pages which the user can
 * scroll/swipe between.
 */
class IntroductionActivity : FragmentActivity() {

    private var viewPager: ViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_introduction)
        viewPager = findViewById(R.id.viewpager)
        viewPager!!.adapter = IntroAdapter(this, supportFragmentManager)
        viewPager!!.setPageTransformer(false, IntroPageTransformer(this))
        val tabLayout = findViewById<TabLayout>(R.id.tabDots)
        tabLayout.setupWithViewPager(viewPager, true)
    }

    /**
     * Once the user finishes the setup the completion is stored in @[SharedPreferences] so this
     * activity won't be called again. In future versions it might be possible to restart this
     * activity from a settings menu, but currently this can be done by clicking on the settings-icon
     * on the toolbar of @[MainActivity].
     */
    fun finishSetup() {
        val shared: SharedPreferences
        shared = getSharedPreferences("me.richardboehm.ocrreader", Context.MODE_PRIVATE)
        shared.edit().putBoolean("first_time", false).apply()
        val intent = Intent(applicationContext,
                MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun previous(view: View) {
        viewPager!!.setCurrentItem(viewPager!!.currentItem - 1, true)
    }

    fun next(view: View) {
        viewPager!!.setCurrentItem(viewPager!!.currentItem + 1, true)
    }
}
