package me.richardboehm.ocrreader.ui.overview

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import me.richardboehm.ocrreader.R
import me.richardboehm.ocrreader.models.Receipt
import java.text.NumberFormat
import java.text.SimpleDateFormat

/**
 * Adapter for the list view of receipts.
 */
internal class ReceiptAdapter(context: Context, private val receipts: List<Receipt>) : ArrayAdapter<Receipt>(context, -1, receipts) {

    /**
     * Implements view holder pattern to create or reuse existing views. Basically fills a view
     * with the correct information.
     */
    @SuppressLint("SimpleDateFormat")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val viewHolder: ViewHolderItem

        // The convertView argument is essentially a "ScrapView" as described is Lucas post
        // http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
        // It will have a non-null value when ListView is asking you recycle the row layout.
        // So, when convertView is not null, you should simply update its contents instead of inflating a new row    layout.

        if (convertView == null) {

            // inflate the layout
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.fragment_receipt, parent, false)

            // well set up the ViewHolder
            viewHolder = ViewHolderItem()
            viewHolder.tvName = convertView!!.findViewById(R.id.tv_name)
            viewHolder.tvTime = convertView.findViewById(R.id.tv_time)
            viewHolder.tvTotal = convertView.findViewById(R.id.tv_total)

            // store the holder with the view.
            convertView.tag = viewHolder

        } else {
            // we've just avoided calling findViewById() on resource everytime
            // just use the viewHolder
            viewHolder = convertView.tag as ViewHolderItem
        }

        // object item based on the position
        val objectItem = receipts[position]

        // assign values if the object is not null
        if (objectItem != null) {
            val format = NumberFormat.getCurrencyInstance()
            // get the TextView from the ViewHolder and then set the text (item name) and tag (item ID) values
            viewHolder.tvName!!.text = SimpleDateFormat("E \t dd.MM").format(objectItem.datetime)
            viewHolder.tvTime!!.text = SimpleDateFormat("H:m").format(objectItem.datetime)
            viewHolder.tvTotal!!.text = format.format(receipts[position].total.toDouble()).toString()
        }

        return convertView
    }

    internal class ViewHolderItem {
        var tvName: TextView? = null
        var tvTime: TextView? = null
        var tvTotal: TextView? = null
    }
}
