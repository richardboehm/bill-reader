/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.richardboehm.ocrreader.ui.camera.ocr;


import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.Line;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.richardboehm.ocrreader.ui.camera.GraphicOverlay;
import me.richardboehm.ocrreader.ui.camera.OcrCaptureActivity;

/**
 * A very simple Processor which gets detected TextBlocks and adds them to the overlay
 * as OcrGraphics.
 */
public class OcrDetectorProcessor implements Detector.Processor<TextBlock> {

    private final GraphicOverlay<OcrGraphic> mGraphicOverlay;
    private OcrCaptureActivity ocr;
    public HashMap<Integer, List<Text>> ReceiptData;

    public OcrDetectorProcessor(GraphicOverlay<OcrGraphic> ocrGraphicOverlay, OcrCaptureActivity ocrCaptureActivity) {
        mGraphicOverlay = ocrGraphicOverlay;
        this.ocr = ocrCaptureActivity;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void receiveDetections(Detector.Detections<TextBlock> detections) {
        mGraphicOverlay.clear();
        ReceiptData = new HashMap<>();
        SparseArray<TextBlock> items = detections.getDetectedItems();
        for (int i = 0; i < items.size(); ++i) {
            TextBlock item = items.valueAt(i);
            List<Line> temporalLines= (List<Line>) item.getComponents();
            Object index = -1;
            for (Line line : temporalLines) {
                if (line != null && line.getValue() != null) {
                    Log.d("Processor", "Text detected! " + line.getValue());
                    index = addToMap(ReceiptData, line);
                }
                OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, ReceiptData.get(index), ocr.getOffset(), ocr.getResources().getConfiguration().orientation);
                mGraphicOverlay.add(graphic);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private int addToMap(Map map, Line line){
        int bottom =  line.getBoundingBox().bottom - (line.getBoundingBox().bottom % 10);
        Object index = null;
        for(Object key : map.keySet()){
            int i = (int) key;
            if(Math.abs(bottom - i) < 15){
                index = key;
            }
        }

        List<Text> textArray;
        if(index != null) {
            textArray = (List<Text>) map.get(index);
            textArray.addAll(line.getComponents());
        } else{
            textArray = (List<Text>) line.getComponents();
            index = bottom;
        }

        map.put(index, textArray);
        return (int) index;
    }

    @Override
    public void release() {
        mGraphicOverlay.clear();
    }
}
