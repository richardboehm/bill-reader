package me.richardboehm.ocrreader.ui.overview;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.text.SimpleDateFormat;
import java.util.List;

import me.richardboehm.ocrreader.R;
import me.richardboehm.ocrreader.models.Receipt;
import me.richardboehm.ocrreader.ui.details.DetailListActivity;
import me.richardboehm.ocrreader.util.DatabaseHelper;

/**
 * Implements @{@link android.app.ListFragment}. Basically handles what happens if you click on a
 * list item.
 */
public class ReceiptFragment extends ListFragment implements AdapterView.OnItemClickListener {

    private List<Receipt> receipts;

    /**
     * Inflate view.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_view, container, false);
        updateAdapter();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setOnItemClickListener(this);

    }

    /**
     * If the user clicks on an item the receiptId and the receiptDate are bundled with an intent
     * to start @{@link DetailListActivity}. The receiptId is necessary to fetch the receipt items
     * from the database. The date is only used in the toolbar for the user to easily identify if
     * he clicked on the correct item. By providing the date we save an additional database call.
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(),
                DetailListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putLong("receiptId", receipts.get(position).getId());
        bundle.putString("receiptDate", new SimpleDateFormat("E \t dd.MM").format(receipts.get(position).getDatetime()));
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * Fetches all receipts from the database and returns them in a list.
     * @return List of receipts.
     */
    private List<Receipt> getReceipts(){
        DatabaseHelper database = new DatabaseHelper(getActivity().getApplicationContext());
        List<Receipt> result= database.getReceipts();
        database.close();
        Log.d("ListFragment", String.format("Found %s receipt(s).", result.size()));
        for(Receipt r : result){
            Log.d("ListFragment", " - " + r.getDatetime().toString());
        }
        return result;
    }

    /**
     * Updates the adapter when a new receipt is added or an existing is removed.
     */
    public void updateAdapter() {
        receipts = getReceipts();
        ReceiptAdapter adapter = new ReceiptAdapter(getActivity().getApplicationContext(), receipts);
        setListAdapter(adapter);
    }
}
