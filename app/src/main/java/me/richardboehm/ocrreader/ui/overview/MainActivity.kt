package me.richardboehm.ocrreader.ui.overview

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.FileProvider.getUriForFile
import android.support.v7.app.AppCompatCallback
import android.support.v7.app.AppCompatDelegate
import android.support.v7.view.ActionMode
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import me.richardboehm.ocrreader.R
import me.richardboehm.ocrreader.models.Category
import me.richardboehm.ocrreader.models.Receipt
import me.richardboehm.ocrreader.models.ReceiptItem
import me.richardboehm.ocrreader.ui.camera.OcrCaptureActivity
import me.richardboehm.ocrreader.ui.intro.IntroductionActivity
import me.richardboehm.ocrreader.util.DatabaseHelper
import java.io.*
import java.util.*
import java.util.Map

/**
 * @[MainActivity] starts after the user finishes the introduction. Provides a list of receipts and
 * means to import, export or delete them.
 */
class MainActivity : Activity(), AppCompatCallback {
    private var delegate: AppCompatDelegate? = null
    private var toolbar: Toolbar? = null
    private var frg: ReceiptFragment? = null

    /**
     * Creates a CSV-file, basically a textfile with data (columns) separated by commas and lines.
     * Each new line is a new dataset (row), whereas the first one provides the column names. The
     * resulting file is written to 'getExternalFilesDir', which is basically the
     * /data/data/me.richardboehm.ocrreader/ folder on the external storage.
     * @return {@code true} if the flash mode is set, {@code false} otherwise
     */
    private val exportFile: File
        get() {
            val header = arrayOf(ReceiptItem.ReceiptItemEntry.COLUMN_NAME_NAME, ReceiptItem.ReceiptItemEntry.COLUMN_NAME_COST, ReceiptItem.ReceiptItemEntry.COLUMN_NAME_CATEGORY, ReceiptItem.ReceiptItemEntry.COLUMN_NAME_RECEIPT, Receipt.ReceiptEntry.COLUMN_NAME_DATETIME)
            val database = DatabaseHelper(this)
            val receipts = database.receipts
            var items: List<ReceiptItem>
            val results = ArrayList<String>()
            results.add(TextUtils.join(",", header))

            for (receipt in receipts) {
                var s: String
                items = database.getItems(receipt.id, "ASC")
                for (item in items) {
                    s = TextUtils.join(",", arrayOf(item.name.replace(",", "."), item.cost.toString(), Category.getCategory(item.category), item.receiptId.toString(), receipt.datetime.toString()))
                    results.add(s)
                }
            }


            val file = File(getExternalFilesDir(null), "receipts.csv")
            Log.d(TAG, "Wrote CSV to " + file.absolutePath)
            var writer: BufferedWriter? = null
            try {
                writer = BufferedWriter(FileWriter(file))
                writer.write(TextUtils.join("\n", results))
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                try {
                    writer!!.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
            return file
        }

    /**
     * When the activity gets created we set the delegate to handle the toolbar.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        delegate = AppCompatDelegate.create(this, this)
        delegate!!.onCreate(savedInstanceState)
        delegate!!.setContentView(R.layout.activity_main)
        toolbar = findViewById(R.id.toolbar)
        toolbar!!.setTitle(R.string.main_title)
        frg = fragmentManager.findFragmentById(R.id.fragment_list) as ReceiptFragment
    }


    /**
     * As activity doesn't have a toolbar, we need to create it manually. If the user pauses
     * and resumes the activity, either by switching to another within activity within or
     * outside the app, the toolbar and the options menu won't be re-rendered. The solution is
     * to set the toolbar  after the activity has been reopened (so onResume) and to invalidate
     * the options menu, because 'onCreateOptionsMenu' only gets called if you create the
     * activity.
     */
    override fun onResume() {
        super.onResume()
        delegate!!.setSupportActionBar(toolbar)
        // if a fragment use hasOptionMenu(true);
        invalidateOptionsMenu()
        reloadFragment()
    }

    /**
     * Opens the @[OcrCaptureActivity] with the 'CAMERA_REQUEST' request code. We are waiting for
     * data from the camera to parse it.
     */
    fun openCamera(view: View) {
        val intent = Intent(this@MainActivity,
                OcrCaptureActivity::class.java)
        startActivityForResult(intent, CAMERA_REQUEST)
    }

    /**
     * When we receive data from an activity we need to parse it. Two types of request codes are
     * currently supported, first the CAMERA_REQUEST which provides the raw data from the OCR.
     * Second the PICKFILE_REQUEST_CODE which provides data from a selected file. Received data is
     * parsed and imported into the database.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("MainActivity", "Received result: $resultCode")
        // Check which request we're responding to
        if (requestCode == CAMERA_REQUEST) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                // Reattach fragment to load new data
                if (data != null) {
                    val result = data.getSerializableExtra("result") as List<List<String>>
                    parseData(result)
                    reloadFragment()
                }
            }
        } else if (requestCode == PICKFILE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val uri = data.data
                importFile(File(uri!!.path))
            }
        }
    }

    /**
     * Imports a file in the csv format. Splits the lines by columns and imports the data into the
     * database.
     * @param file The file to be imported.
     */
    private fun importFile(file: File) {
        //File file = new File(getExternalFilesDir(null), "receipts.csv");
        val reader: BufferedReader
        val database = DatabaseHelper(this)
        try {
            reader = BufferedReader(FileReader(file))
            val headerLine = reader.readLine()
            val items = HashMap<String, List<ReceiptItem>>()
            val nameIndex: Int
            val costIndex: Int
            val categoryIndex: Int
            val receiptIndex: Int
            val dateTimeIndex: Int

            if (headerLine != null) {
                val header = headerLine.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                nameIndex = findIndex(header, ReceiptItem.ReceiptItemEntry.COLUMN_NAME_NAME)
                costIndex = findIndex(header, ReceiptItem.ReceiptItemEntry.COLUMN_NAME_COST)
                categoryIndex = findIndex(header, ReceiptItem.ReceiptItemEntry.COLUMN_NAME_CATEGORY)
                receiptIndex = findIndex(header, ReceiptItem.ReceiptItemEntry.COLUMN_NAME_RECEIPT)
                dateTimeIndex = findIndex(header, Receipt.ReceiptEntry.COLUMN_NAME_DATETIME)

                var itemRow: Array<String>
                var line: String? = reader.readLine()
                while (line != null) {
                    itemRow = line.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val receiptId = java.lang.Long.parseLong(itemRow[receiptIndex])
                    val item = ReceiptItem(itemRow[nameIndex], java.lang.Float.parseFloat(itemRow[costIndex]), Category.getCategory(itemRow[categoryIndex]), receiptId)
                    database.insertItem(item)
                    Log.d(TAG, item.toString())
                    var currentItems: MutableList<ReceiptItem>? = items[itemRow[receiptIndex]] as MutableList<ReceiptItem>?
                    if (currentItems == null) {
                        currentItems = ArrayList()
                        database.insertReceipt(Receipt(receiptId, java.lang.Long.parseLong(itemRow[dateTimeIndex]), 0.0f))
                    }
                    currentItems.add(item)
                    items[itemRow[receiptIndex]] = currentItems
                    line = reader.readLine()
                }

                val it = items.entries.iterator()
                while (it.hasNext()) {
                    val pair = it.next() as Map.Entry<*, *>
                    val receiptId = java.lang.Long.parseLong(pair.key as String)
                    database.updateReceipt(receiptId, database.getTotal(receiptId))
                    it.remove()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        reloadFragment()
    }

    /**
     * Parses the data and inserts it into the database. An entry in the list contains a list of
     * strings these represent the whole item row. The item row may or may not have information
     * about the article name, cost, or other units. The design of receipts is often quite different
     * so we assume that each detected row has cost, we try to parse each string as float, starting
     * from the end (or right).
     * If one sticks that is probably our item cost. Afterwards we just concatenate the remaining
     * strings and get the item name. The data is then entered into the database.
     * @param result The list contains rows of data. These rows are lists of Strings.
     * @see @[me.richardboehm.ocrreader.ui.camera.ocr.OcrDetectorProcessor]
     */
    private fun parseData(result: List<List<String>>?) {
        if (result != null) {
            val database = DatabaseHelper(applicationContext)

            val receiptId = database.insertReceipt()
            var total = 0.0f
            for (line in result) {

                val costIndex = findValueIndex(line)
                var value = 0.00f
                if (costIndex > -1)
                    value = tryParseFloat(line[costIndex])!!

                total += value
                val builder = StringBuilder()
                for (i in 0 until costIndex) {
                    if (builder.length > 0) {
                        builder.append(" ")
                    }
                    builder.append(line[i])
                }

                val itemString = builder.toString()
                database.insertItem(ReceiptItem(itemString, value, Category.UNDEFINED, receiptId))
            }
            val receipt = Receipt(receiptId, Date().time, total)
            database.updateReceipt(receipt)
        }
    }


    /**
     * We design of receipts is often very different, so we can not assume that the string on the
     * very right is indeed the cost of the item. We try to parse all items as float, if one sticks
     * we return the index of that string.
     * @param line List of strings.
     * @return Returns the index of the presumed float.
     */
    private fun findValueIndex(line: List<String>): Int {
        var index = -1
        for (i in line.indices) {
            if (tryParseFloat(line[i]) != null) {
                index = i
            }
        }
        return index
    }

    /**
     * Try to parse a string as float. We replace the ',' with '.' and if the resulting string can
     * be parsed as float we return that, otherwise we return null.
     * @param s String to parse
     * @return Float or null
     */
    private fun tryParseFloat(s: String): Float? {
        var s = s
        try {
            // If there is no decimal separator in the string, the recognition probably wasn't correct.
            // By appending the string to "0.", we are forcing a decimal value. We might run into
            // issues as "0." is a valid float.
            s = s.replace(',', '.')
            s = if (s.indexOf('.') < 0) "0.$s" else s
            return java.lang.Float.parseFloat(s)
        } catch (e: Exception) {
            // Logging of error not necessary as we expect most values to be invalid.
            return null
        }

    }

    /**
     * In case the adapter does not update itself.
     */
    private fun reloadFragment() {
        frg!!.updateAdapter()
    }

    /**
     * Clears the database, deleting all entries.
     */
    private fun clearDatabase() {
        val database = DatabaseHelper(application.applicationContext)
        database.onUpgrade(database.writableDatabase, 0, 0)
        database.close()
        reloadFragment()
    }

    /**
     * Create an overflow menu.
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return true
    }

    /**
     * Handles interaction with the overflow menu.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this@MainActivity, IntroductionActivity::class.java)
                startActivity(intent)
                return true
            }
        /*case R.id.action_charts:
                Intent intent = new Intent(MainActivity.this, IntroductionActivity.class);
                startActivity(intent);
                return true;*/
            R.id.action_clear -> {
                clearDatabase()
                return true
            }
            R.id.action_import -> {
                importCSV()
                return true
            }
            R.id.action_export -> {
                exportCSV()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    /**
     * Export the created CSV with the help of a Fileprovider. Requires one to be installed.
     */
    private fun exportCSV() {
        val intent = Intent(Intent.ACTION_SEND)
        val contentUri = getUriForFile(this, "$packageName.provider", exportFile)
        intent.setDataAndType(contentUri, "text/csv")
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(intent, "Share file with"))
    }

    /**
     * Imports the file with the help of a Fileprovider. Requires one to be installed.
     */
    private fun importCSV() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "text/csv"
        intent.addCategory(Intent.CATEGORY_DEFAULT)
        startActivityForResult(intent, PICKFILE_REQUEST_CODE)
    }

    /**
     * Finds the index of a key in an array.
     */
    fun findIndex(array: Array<String>, key: String): Int {
        var index = -1
        for (i in array.indices) {
            if (array[i] == key)
                index = i
        }
        return index
    }

    override fun onSupportActionModeStarted(mode: ActionMode) {}

    override fun onSupportActionModeFinished(mode: ActionMode) {}

    override fun onWindowStartingSupportActionMode(callback: ActionMode.Callback): ActionMode? {
        return null
    }

    companion object {
        private val TAG = MainActivity::class.java.name
        private val CAMERA_REQUEST = 1
        private val PICKFILE_REQUEST_CODE = 2
    }
}
