/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.richardboehm.ocrreader.ui.camera.ocr;

import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import com.google.android.gms.vision.text.Text;

import java.util.List;

import me.richardboehm.ocrreader.ui.camera.GraphicOverlay;

/**
 * Graphic instance for rendering TextBlock position, size, and ID within an associated graphic
 * overlay view.
 * @version 1.0
 */
public class OcrGraphic extends GraphicOverlay.Graphic {

    private int mId;

    private static final String TAG = OcrGraphic.class.getName();
    private static final int TEXT_COLOR = Color.WHITE;

    private static Paint sRectPaint;
    private static Paint sTextPaint;
    private final List<Text> mText;
    private float[] offset;
    private int orientation;

    OcrGraphic(GraphicOverlay overlay, List<Text> text, float[] offset, int orientation) {
        super(overlay);
        this.offset = offset;
        this.orientation = orientation;
        mText = text;

        if (sRectPaint == null) {
            sRectPaint = new Paint();
            sRectPaint.setColor(TEXT_COLOR);
            sRectPaint.setStyle(Paint.Style.STROKE);
            sRectPaint.setStrokeWidth(4.0f);
        }

        if (sTextPaint == null) {
            sTextPaint = new Paint();
            sTextPaint.setColor(TEXT_COLOR);
            sTextPaint.setTextSize(54.0f);
        }
        // Redraw the overlay, as this graphic has been added.
        postInvalidate();
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public List<Text> getTextBlock() {
        return mText;
    }

    /**
     * Checks whether a point is within the bounding box of this graphic.
     * The provided point should be relative to this graphic's containing overlay.
     * @param x An x parameter in the relative context of the canvas.
     * @param y A y parameter in the relative context of the canvas.
     * @return True if the provided point is contained within this graphic's bounding box.
     */
    public boolean contains(float x, float y) {
        return false;
    }

    /**
     * Draws the text block annotations for position, size, and raw value on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        if (mText == null) {
            return;
        }

        // Draws a box around the element.
        RectF rectFirst = new RectF(mText.get(0).getBoundingBox());
        RectF rectLast = new RectF(mText.get(mText.size()-1).getBoundingBox());
        switch (orientation) {
            case (Configuration.ORIENTATION_PORTRAIT): {
                rectFirst.left = translateX(rectFirst.left + offset[1]);
                rectFirst.top = translateY(rectFirst.top + offset[0]);
                rectFirst.right = translateX(rectLast.right + offset[1]);
                rectFirst.bottom = translateY(rectFirst.bottom + offset[0]);
                break;
            }
            default: {
                rectFirst.left = translateX(rectFirst.left + offset[0]);
                rectFirst.top = translateY(rectFirst.top + offset[1]);
                rectFirst.right = translateX(rectLast.right + offset[0]);
                rectFirst.bottom = translateY(rectFirst.bottom + offset[1]);
            }
        }
        canvas.drawRect(rectFirst, sRectPaint);

        // Break the text into multiple words and draw each one according to its own bounding box.
        for(Text currentText : mText) {
            float left, bottom;
            switch (orientation) {
                case (Configuration.ORIENTATION_PORTRAIT): {
                    left = translateX(currentText.getBoundingBox().left + offset[1]);
                    bottom = translateY(currentText.getBoundingBox().bottom + offset[0]);
                    break;
                }
                default: {
                    left = translateX(currentText.getBoundingBox().left + offset[0]);
                    bottom = translateY(currentText.getBoundingBox().bottom + offset[1]);
                }
            }
            canvas.drawText(currentText.getValue(), left, bottom, sTextPaint);
        }
    }
}
