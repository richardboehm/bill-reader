package me.richardboehm.ocrreader.ui.details

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import me.richardboehm.ocrreader.R
import me.richardboehm.ocrreader.models.Category
import me.richardboehm.ocrreader.models.ReceiptItem
import java.text.NumberFormat
import java.util.*

/**
 * ArrayAdapter for @[ReceiptItem]s  can be filtered by a category to limit the amount of items
 * displayed.
 */
class ItemAdapter(context: Context, items: List<ReceiptItem>) : ArrayAdapter<ReceiptItem>(context, -1, items), Filterable {
    private val items: List<ReceiptItem>
    private var filtered: List<ReceiptItem>? = null
    private val filter = ItemFilter()

    init {
        this.items = ArrayList(items)
        this.filtered = ArrayList(items)
    }

    /**
     * Implements the viewholder pattern to create or reuse existing views and filling them with
     * correct information.
     */
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var convertView = convertView
        val viewHolder: ViewHolderItem

        if (convertView == null) {

            // inflate the layout
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.fragment_item, parent, false)

            // well set up the ViewHolder
            viewHolder = ViewHolderItem()
            viewHolder.tvName = convertView!!.findViewById(R.id.tv_name)
            viewHolder.tvCategory = convertView.findViewById(R.id.tv_item_category)
            viewHolder.tvCost = convertView.findViewById(R.id.tv_item_cost)

            // store the holder with the view.
            convertView.tag = viewHolder

        } else {
            // we've just avoided calling findViewById() on resource everytime
            // just use the viewHolder
            viewHolder = convertView.tag as ViewHolderItem
        }

        // object item based on the position
        val objectItem = filtered!![position]

        // assign values if the object is not null
        if (objectItem != null) {
            val format = NumberFormat.getCurrencyInstance()
            // get the TextView from the ViewHolder and then set the text (item name) and tag (item ID) values
            viewHolder.tvName!!.text = objectItem.name
            viewHolder.tvCategory!!.text = Category.getCategory(objectItem.category)
            viewHolder.tvCost!!.text = format.format(objectItem.cost.toDouble())
        }

        return convertView
    }

    /**
     * Returns the @[ItemFilter].
     */
    override fun getFilter(): Filter {
        return filter
    }

    /**
     * Returns the @[ReceiptItem] by its position in the filtered list.
     */
    override fun getItem(pos: Int): ReceiptItem? {
        return filtered!![pos]
    }

    /**
     * List of items in the filtered list.
     */
    override fun getCount(): Int {
        return filtered!!.size
    }

    /**
     * Filters the list of items according to the selected category in the pie chart.
     */
    private inner class ItemFilter : Filter() {

        override fun performFiltering(constraint: CharSequence): Filter.FilterResults {
            var constraint = constraint
            // NOTE: this function is *always* called from a background thread, and
            // not the UI thread.
            constraint = constraint.toString().toLowerCase()
            val result = Filter.FilterResults()
            if (constraint.toString().length > 0) {
                val filter = ArrayList<ReceiptItem>()
                for (item in items) {
                    if (item.category.toString() == constraint)
                        filter.add(item)
                }
                result.count = filter.size
                result.values = filter
            } else {
                filtered = ArrayList(items)
                synchronized(this) {
                    result.values = items
                    result.count = items.size
                }
            }
            return result
        }

        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
            filtered = results.values as ArrayList<ReceiptItem>
            notifyDataSetChanged()
            var i = 0
            val l = filtered!!.size
            while (i < l) {
                add(filtered!![i])
                i++
            }
            Log.d("Chart", filtered!!.size.toString())
            notifyDataSetInvalidated()
        }

    }

    internal class ViewHolderItem {
        var tvName: TextView? = null
        var tvCategory: TextView? = null
        var tvCost: TextView? = null
    }
}
