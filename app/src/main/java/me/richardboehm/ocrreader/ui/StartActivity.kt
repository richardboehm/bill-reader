package me.richardboehm.ocrreader.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log

import me.richardboehm.ocrreader.ui.intro.IntroductionActivity
import me.richardboehm.ocrreader.ui.overview.MainActivity

/**
 * This is the first activity which gets called when the user opens the app. If the user opens the
 * application for the first time this activity starts an intent to open the [IntroductionActivity],
 * if the user already progressed through the [IntroductionActivity] then this acitivty starts
 * the [MainActivity].
 */
class StartActivity : Activity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val shared: SharedPreferences
        shared = getSharedPreferences("me.richardboehm.ocrreader", Context.MODE_PRIVATE)
        Log.d(TAG, "Starting: " + shared.getBoolean("first_time", false))
        if (shared.getBoolean("first_time", true)) {
            startActivity(Intent(this, IntroductionActivity::class.java))
        } else {
            startActivity(Intent(this, MainActivity::class.java))
        }
        finish()
    }

    companion object {

        private val TAG = StartActivity::class.java.name
    }
}
