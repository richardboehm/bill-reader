package me.richardboehm.ocrreader.ui.details;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.ListFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.richardboehm.ocrreader.R;
import me.richardboehm.ocrreader.models.Category;
import me.richardboehm.ocrreader.models.ReceiptItem;
import me.richardboehm.ocrreader.util.CurrencyValueFormatter;
import me.richardboehm.ocrreader.util.DatabaseHelper;

/**
 * Implements the ListFragment to display all items of a receipt.
 */
public class DetailListFragment extends ListFragment implements AdapterView.OnItemClickListener{

    private List<ReceiptItem> items;
    public ItemAdapter adapter;
    private Long receiptId;
    private OnTotalPass dataPasser;
    private Activity activity;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        this.activity = activity;
        dataPasser = (OnTotalPass) activity;
    }

    /**
     * Updates the item with the provided parameters. After that the corresponding entry in the
     * database and the chart are updated.
     *
     * @param position Position of the item in the list.
     * @param name     New name of the item.
     * @param cost     New cost of the item.
     * @param category New category of the item.
     */
    public void updateItem(int position, String name, float cost, int category) {
        ReceiptItem item = items.get(position);
        item.setName(name);
        item.setCost(cost);
        item.setCategory(category);
        DatabaseHelper database = new DatabaseHelper(getActivity().getApplicationContext());
        database.updateItem(item);
        updateChart();
    }

    /**
     * Passes the data to the activity.
     */
    public void passTotal() {
        dataPasser.onTotalPass(getTotal());
    }

    /**
     * After the activity is created we fetch the items and update the pie chart.
     * @param savedInstanceState
     * @see PieChart
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        receiptId = getActivity().getIntent().getLongExtra("receiptId", -1);
        items = getReceiptItems();
        adapter = new ItemAdapter(getActivity().getApplicationContext(), items);
        passTotal();

        updateChart();

        setListAdapter(adapter);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_view, container, false);
    }

    /**
     * If the data set changes we need to update the pie chart to reflect that change.
     * This also provides means to filter the list of items according to the selected category in
     * the pie chart.
     * @see PieChart
     */
    private void updateChart(){
        PieData data = new PieData(getChartData(items));
        PieChart chart = getActivity().findViewById(R.id.category_chart);
        chart.setData(data);
        Legend legend = chart.getLegend();
        legend.setEnabled(false);
        chart.setMinOffset(10);
        chart.getDescription().setEnabled(false);
        chart.setHoleColor(Color.TRANSPARENT);
        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                adapter.getFilter().filter(String.valueOf(Category.INSTANCE.getCategory(((PieEntry) e).getLabel())));
            }

            @Override
            public void onNothingSelected() {
                adapter.getFilter().filter("");
            }
        });
        chart.invalidate(); // refresh
    }

    /**
     * Parses the receipt item data for the pie chart. The chart uses a HashMap with the Categories
     * as keys and the total cost of a category as entry.
     * @param receiptItems List of items
     * @return Returns a {@link PieDataSet} for the pie chart.
     * @se @{@link PieChart}
     */
    private PieDataSet getChartData(List<ReceiptItem> receiptItems){
        HashMap<Integer, Float> items = new HashMap<>();
        List<PieEntry> entries = new ArrayList<>();

        // Accumulate data
        for(ReceiptItem data: receiptItems){
            float value = items.get(data.getCategory()) != null ? items.get(data.getCategory()) : 0.0f;
            value += data.getCost();
            items.put(data.getCategory(), value);
        }

        // Add accumulated values to the data set
        for (Map.Entry<Integer, Float> entry : items.entrySet()) {
            entries.add(new PieEntry(entry.getValue(), Category.INSTANCE.getCategory(entry.getKey())));
        }

        ContextCompat.getColor(activity, R.color.red);
        PieDataSet dataSet = new PieDataSet(entries, "Categories");
        dataSet.setColors(ContextCompat.getColor(activity, R.color.red),
                ContextCompat.getColor(activity, R.color.green),
                ContextCompat.getColor(activity, R.color.blue),
                ContextCompat.getColor(activity, R.color.orange));
        dataSet.setValueTextSize(10.0f);
        dataSet.setValueTextColor(ContextCompat.getColor(activity, R.color.colorPrimaryText));
        dataSet.setValueFormatter(new CurrencyValueFormatter());
        return dataSet;
    }

    public List<String> getChartLabels(List<ReceiptItem> receiptItems) {
        List<String> labels = new ArrayList<>();
        for (ReceiptItem data : receiptItems) {
            labels.add(String.valueOf(data.getCategory()));
        }
        return labels;
    }

    /**
     * If the user clicks on an item a dialog opens which provides means to update the selected item.
     * @param parent ParentView.
     * @param view View.
     * @param position position of the item in the list.
     * @param id id.
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Toast.makeText(getActivity(), "Item: " + items.get(position).name, Toast.LENGTH_SHORT).show();

        DialogFragment dialog = new ItemDialogFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putString("name", items.get(position).getName());
        args.putInt("category", items.get(position).getCategory());
        args.putFloat("cost", items.get(position).getCost());

        dialog.setArguments(args);
        dialog.setTargetFragment(this, 1);
        dialog.show(getFragmentManager(), "UpdateItem");
    }

    /**
     * Fetches all @{@link ReceiptItem}s as a list.
     * @return Returns a list of {@link ReceiptItem}s
     */
    private List<ReceiptItem> getReceiptItems(){
        DatabaseHelper database = new DatabaseHelper(getActivity().getApplicationContext());
        return database.getItems(receiptId, "ASC");
    }

    /**
     * Calculates the total cost of all items.
     * @return Float representation of the total cost.
     */
    public float getTotal(){
        float total = 0.0f;
        for(ReceiptItem item : items){
            total += item.getCost();
        }
        return total;
    }

    /**
     * Interface to pass the total sum to the activity.
     */
    public interface OnTotalPass {
        void onTotalPass(float data);
    }

}
