package me.richardboehm.ocrreader.models

/**
 * Helper class to support the '_ID' field provided by @[android.provider.BaseColumns] in Kotlin.
 */
open class KBaseColumns {
    val _ID = "_ID"
}