package me.richardboehm.ocrreader.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.richardboehm.ocrreader.models.Category;
import me.richardboehm.ocrreader.models.Receipt;
import me.richardboehm.ocrreader.models.Receipt.ReceiptEntry;
import me.richardboehm.ocrreader.models.ReceiptItem;
import me.richardboehm.ocrreader.models.ReceiptItem.ReceiptItemEntry;

/**
 * Helper class to manage database handling. Creates and deletes tables, provides means to insert,
 * update or delete entries within tables.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "Database";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Receipts.db";

    private static final String SQL_CREATE_RECEIPTS = String.format("CREATE TABLE %s (%s INTEGER, %s TEXT, %s REAL, PRIMARY KEY(%s));",
            ReceiptEntry.Companion.getTABLE_NAME(), ReceiptEntry._ID, ReceiptEntry.Companion.getCOLUMN_NAME_DATETIME(), ReceiptEntry.Companion.getCOLUMN_NAME_TOTAL(), ReceiptEntry._ID);
    private static final String SQL_CREATE_ITEMS = String.format(
            "CREATE TABLE %s (%s INTEGER PRIMARY KEY,%s TEXT, %s REAL, %s INTEGER, %s INTEGER, FOREIGN KEY(%s) REFERENCES %s(%s));",
            ReceiptItemEntry.Companion.getTABLE_NAME(),
            ReceiptItemEntry._ID,
            ReceiptItemEntry.Companion.getCOLUMN_NAME_NAME(),
            ReceiptItemEntry.Companion.getCOLUMN_NAME_COST(),
            ReceiptItemEntry.Companion.getCOLUMN_NAME_CATEGORY(),
            ReceiptItemEntry.Companion.getCOLUMN_NAME_RECEIPT(),
            ReceiptItemEntry.Companion.getCOLUMN_NAME_RECEIPT(),
            ReceiptEntry.Companion.getTABLE_NAME(),
            ReceiptEntry._ID);

    private static final String SQL_DELETE_RECEIPTS = String.format("DROP TABLE IF EXISTS %s;", ReceiptEntry.Companion.getTABLE_NAME());
    private static final String SQL_DELETE_ITEMS = String.format("DROP TABLE IF EXISTS %s;", ReceiptItemEntry.Companion.getTABLE_NAME());

    private static final String SQL_CREATE_ENTRIES = String.format("%s \n %s", SQL_CREATE_RECEIPTS, SQL_CREATE_ITEMS);

    private SQLiteDatabase database;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        database = getWritableDatabase(); // https://stackoverflow.com/a/6791968
    }


    /**
     * Creates the databases receipts and items if they don't yet exist
     *
     * @param sqLiteDatabase
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_RECEIPTS);
        sqLiteDatabase.execSQL(SQL_CREATE_ITEMS);
        Log.d(TAG, String.format("Database created. \n%s", SQL_CREATE_ENTRIES));
    }

    /**
     * If the tables exist and the version is below the current one, the previous tables are dropped
     * and newly created.
     * @param sqLiteDatabase
     * @param i
     * @param i1
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_ITEMS);
        sqLiteDatabase.execSQL(SQL_DELETE_RECEIPTS);
        Log.d(TAG, "Database is updating.");
        onCreate(sqLiteDatabase);
    }

    /**
     * Identical as onUpgrade.
     * @param sqLiteDatabase
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onDowngrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }

    /**
     * Updates the database entry with the given item. The id of the entry is taken from the
     * parameter item. Requires the item to have an id.
     * @param item Updated item.
     */
    public void updateItem(ReceiptItem item){
        database = getWritableDatabase();
        String id = "_id='" + item.getId() + "'";
        ContentValues values = new ContentValues();
        values.put(ReceiptItemEntry.Companion.getCOLUMN_NAME_NAME(), item.getName());
        values.put(ReceiptItemEntry.Companion.getCOLUMN_NAME_COST(), item.getCost());
        values.put(ReceiptItemEntry.Companion.getCOLUMN_NAME_CATEGORY(), item.getCategory());
        values.put(ReceiptItemEntry.Companion.getCOLUMN_NAME_RECEIPT(), item.getReceiptId());
        database.update(ReceiptItemEntry.Companion.getTABLE_NAME(), values, id, null);
        database.close();
    }

    /**
     * Updates the database entry with the given receipt. The id of the entry is taken from the
     * parameter item. Requires the receipt to have an id.
     * @param receipt Updated item.
     */
    public void updateReceipt(Receipt receipt){
        database = getWritableDatabase();
        String id = "_id='" + receipt.getId() + "'";
        ContentValues values = new ContentValues();
        values.put(ReceiptEntry.Companion.getCOLUMN_NAME_TOTAL(), receipt.getTotal());
        database.update(ReceiptEntry.Companion.getTABLE_NAME(), values, id, null);
        database.close();
    }

    /**
     * Updates the database entry with the given data from the parameters. The id of the entry is taken from the
     * parameter id and the total from the corresponding parameter.
     * @param receiptId Id of the receipt.
     * @param total Summed cost of the receipt.
     */
    public void updateReceipt(long receiptId, float total){
        database = getWritableDatabase();
        String id = "_id='" + receiptId + "'";
        ContentValues values = new ContentValues();
        values.put(ReceiptEntry.Companion.getCOLUMN_NAME_TOTAL(), total);
        database.update(ReceiptEntry.Companion.getTABLE_NAME(), values, id, null);
        database.close();
    }

    /**
     * Inserts the receipt into the database.
     * @param receipt Receipt to insert.
     * @return Returns the identifier of the new database entry.
     */
    public long insertReceipt(Receipt receipt) {
        ContentValues values = new ContentValues();
        values.put(ReceiptEntry.Companion.getCOLUMN_NAME_DATETIME(), receipt.getDatetime().toString());
        values.put(ReceiptEntry.Companion.getCOLUMN_NAME_TOTAL(), receipt.getTotal());
        return insert(ReceiptEntry.Companion.getTABLE_NAME(), values);
    }

    /**
     * Createas and inserts a receipt into the database from the given DateTime.
     * @param dateTime The DateTime of the receipt.
     * @return Returns the identifier of the new database entry.
     */
    public long insertReceipt(String dateTime) {
        ContentValues values = new ContentValues();
        values.put(ReceiptEntry.Companion.getCOLUMN_NAME_DATETIME(), dateTime);
        values.put(ReceiptEntry.Companion.getCOLUMN_NAME_TOTAL(), 0.0f);
        return insert(ReceiptEntry.Companion.getTABLE_NAME(), values);
    }

    /**
     * Createas and inserts a receipt into the database from the current DateTime.
     * @return Returns the identifier of the new database entry.
     */
    public long insertReceipt(){
        ContentValues values = new ContentValues();
        Date date = new Date();
        values.put(ReceiptEntry.Companion.getCOLUMN_NAME_DATETIME(), date.getTime());
        values.put(ReceiptEntry.Companion.getCOLUMN_NAME_TOTAL(), 0.0f);
        long id = insert(ReceiptEntry.Companion.getTABLE_NAME(), values);
        Log.d(TAG, String.format("Inserted Receipt: %d", id));
        return id;
    }

    /**
     * Inserts the ReceiptItem into the database.
     * @param item ReceiptItem to insert.
     * @return Returns the identifier of the new database entry.
     */
    public void insertItem(ReceiptItem item){
        ContentValues values = new ContentValues();
        values.put(ReceiptItemEntry.Companion.getCOLUMN_NAME_NAME(), item.getName());
        values.put(ReceiptItemEntry.Companion.getCOLUMN_NAME_COST(), item.getCost());
        values.put(ReceiptItemEntry.Companion.getCOLUMN_NAME_CATEGORY(), item.getCategory());
        values.put(ReceiptItemEntry.Companion.getCOLUMN_NAME_RECEIPT(), item.getReceiptId());
        insert(ReceiptItemEntry.Companion.getTABLE_NAME(), values);
    }

    /**
     * Sums up the cost of all {@link ReceiptItem} in the database, which belong to the given
     * receipt.
     * @param receiptId Identifier of the receipt.
     * @return Returns the total cost of the receipt.
     */
    public float getTotal(long receiptId) {
        float total = 0.0f;
        String[] projection = {
                ReceiptItemEntry.Companion.getCOLUMN_NAME_COST(),
        };
        String selection = String.format("%s=?", ReceiptItemEntry.Companion.getCOLUMN_NAME_RECEIPT());
        String[] selectionArgs = {String.valueOf(receiptId)};
        String sortOrder = String.format("%s %s", ReceiptItemEntry.Companion.getCOLUMN_NAME_NAME(), "ASC");

        database = getReadableDatabase();
        Cursor cursor = database.query(ReceiptItemEntry.Companion.getTABLE_NAME(), projection, selection,
                selectionArgs, null, null, sortOrder);

        while (cursor.moveToNext()) {
            total += cursor.getFloat(
                    cursor.getColumnIndex(ReceiptItemEntry.Companion.getCOLUMN_NAME_COST()));
        }
        cursor.close();
        database.close();
        return total;
    }

    /**
     * Returns the {@link Receipt} by its identifier.
     * @param id Identifier of the {@link Receipt}.
     * @return Returns the {@link Receipt} by its identifier.
     */
    public Receipt getReceipt(long id){
        String[] projection = {
                BaseColumns._ID,
                ReceiptEntry.Companion.getCOLUMN_NAME_DATETIME(),
                ReceiptEntry.Companion.getCOLUMN_NAME_TOTAL()
        };
        String selection = String.format("%s=?", ReceiptEntry._ID);
        String[] selectionArgs = {String.valueOf(id)};
        String sortOrder = String.format("%s DESC", ReceiptEntry.Companion.getCOLUMN_NAME_DATETIME());

        database = getReadableDatabase();
        Cursor cursor = database.query(ReceiptEntry.Companion.getTABLE_NAME(), projection, selection,
                selectionArgs, null, null, sortOrder);
        int dateTimeIndex = cursor.getColumnIndex(ReceiptEntry.Companion.getCOLUMN_NAME_DATETIME());
        int totalIndex = cursor.getColumnIndex(ReceiptEntry.Companion.getCOLUMN_NAME_TOTAL());
        float total = cursor.getFloat(totalIndex);
        cursor.close();
        database.close();
        return new Receipt(id, Long.parseLong(cursor.getString(dateTimeIndex)), total);
    }

    /**
     * Returns the {@link ReceiptItem} by its identifier.
     * @param id Identifier of the {@link ReceiptItem}.
     * @return Returns the {@link ReceiptItem} by its identifier.
     */
    public ReceiptItem getItem(long id){
        String[] projection = {
                BaseColumns._ID,
                ReceiptItemEntry.Companion.getCOLUMN_NAME_NAME(),
                ReceiptItemEntry.Companion.getCOLUMN_NAME_COST(),
                ReceiptItemEntry.Companion.getCOLUMN_NAME_CATEGORY(),
                ReceiptItemEntry.Companion.getCOLUMN_NAME_RECEIPT()
        };
        String selection = String.format("%s=?", ReceiptItemEntry._ID);
        String[] selectionArgs = {String.valueOf(id)};
        String sortOrder = String.format("%s DESC", ReceiptItemEntry.Companion.getCOLUMN_NAME_NAME());

        database = getReadableDatabase();
        Cursor cursor = database.query(ReceiptItemEntry.Companion.getTABLE_NAME(), projection, selection,
                selectionArgs, null, null, sortOrder);

        int nameIndex = cursor.getColumnIndex(ReceiptItemEntry.Companion.getCOLUMN_NAME_NAME());
        int costIndex = cursor.getColumnIndex(ReceiptItemEntry.Companion.getCOLUMN_NAME_COST());
        int receiptIndex = cursor.getColumnIndex(ReceiptItemEntry.Companion.getCOLUMN_NAME_RECEIPT());

        String name = cursor.getString(nameIndex);
        float cost = cursor.getFloat(costIndex);
        long receiptID = cursor.getLong(receiptIndex);

        cursor.close();
        database.close();
        return new ReceiptItem(id, name, cost, Category.INSTANCE.getUNDEFINED(), receiptID);
    }

    /**
     * Returns an {@link ArrayList} of  all {@link Receipt}s
     * @return {@link ArrayList} of  all {@link Receipt}s
     */
    public ArrayList<Receipt> getReceipts(){
        ArrayList<Receipt> receipts = new ArrayList<>();

        String[] projection = {
                BaseColumns._ID,
                ReceiptEntry.Companion.getCOLUMN_NAME_DATETIME(),
                ReceiptEntry.Companion.getCOLUMN_NAME_TOTAL()
        };
        String sortOrder = String.format("%s DESC", ReceiptEntry.Companion.getCOLUMN_NAME_DATETIME());

        database = getReadableDatabase();
        Cursor cursor = database.query(ReceiptEntry.Companion.getTABLE_NAME(), projection, null,
                null, null, null, sortOrder);
        //.rawQuery(String.format("SELECT * FROM %s", ReceiptEntry.TABLE_NAME), null);

        while(cursor.moveToNext()){
            long id = cursor.getLong(
                    cursor.getColumnIndex(ReceiptEntry._ID));
            long date = cursor.getLong(cursor.getColumnIndex(ReceiptEntry.Companion.getCOLUMN_NAME_DATETIME()));
            float total = cursor.getFloat(cursor.getColumnIndex(ReceiptEntry.Companion.getCOLUMN_NAME_TOTAL()));

            Log.d(TAG, String.format("%s \t %s", String.valueOf(id), date));
            receipts.add(new Receipt(id, date, total));
        }

        cursor.close();
        database.close();
        return receipts;
    }

    /**
     * Delete a receipt by its identifier.
     * @param id Identifer of the receipt to delete.
     * @return Return the number of deleted elements.
     */
    public int deleteReceipt(long id) {
        database = getWritableDatabase();
        int deleted = database.delete(ReceiptEntry.Companion.getTABLE_NAME(), ReceiptEntry._ID + "=" + id, null);
        database.close();
        return deleted;
    }

    /**
     * Generic  method to insert entries into a table.
     * @param table Table to enter the data into.
     * @param values Values to enter.
     * @return Returns the identifier of the entry.
     */
    private long insert(String table, ContentValues values){
        database = getWritableDatabase();
        long id = database.insert(table, null, values);
        Log.d(TAG, String.format("Table: %s \t ID: %s \t Values: %s", table, id, values.toString()));
        database.close();
        return id;
    }

    /**
     * Returns a sorted {@link List} of {@link ReceiptItem}s of a given {@link Receipt}.
     * @param receiptId Identifier of the {@link Receipt}
     * @param itemSortOrder Sort order (e.g 'asc', 'desc')
     * @return Returns the list of {@link ReceiptItem}s
     */
    public List<ReceiptItem> getItems(Long receiptId, String itemSortOrder) {

        ArrayList<ReceiptItem> receiptItems = new ArrayList<>();

        String[] projection = {
                BaseColumns._ID,
                ReceiptItemEntry.Companion.getCOLUMN_NAME_NAME(),
                ReceiptItemEntry.Companion.getCOLUMN_NAME_COST(),
                ReceiptItemEntry.Companion.getCOLUMN_NAME_CATEGORY(),
                ReceiptItemEntry.Companion.getCOLUMN_NAME_RECEIPT()
        };
        String selection = String.format("%s=?", ReceiptItemEntry.Companion.getCOLUMN_NAME_RECEIPT());
        String[] selectionArgs = {String.valueOf(receiptId)};
        String sortOrder = String.format("%s %s", ReceiptItemEntry.Companion.getCOLUMN_NAME_NAME(), itemSortOrder);

        database = getReadableDatabase();
        Cursor cursor = database.query(ReceiptItemEntry.Companion.getTABLE_NAME(), projection, selection,
                selectionArgs, null, null, sortOrder);

        while(cursor.moveToNext()){
            long id = cursor.getLong(
                    cursor.getColumnIndex(ReceiptItemEntry._ID));
            String name = cursor.getString(
                    cursor.getColumnIndex(ReceiptItemEntry.Companion.getCOLUMN_NAME_NAME()));
            float cost = cursor.getFloat(
                    cursor.getColumnIndex(ReceiptItemEntry.Companion.getCOLUMN_NAME_COST()));
            int category = cursor.getInt(
                    cursor.getColumnIndex(ReceiptItemEntry.Companion.getCOLUMN_NAME_CATEGORY()));
            receiptItems.add(new ReceiptItem(id, name, cost, category, receiptId));
            Log.d(TAG, String.format("Loaded: %s \t ID: %d Name: %s Cost:%s", ReceiptItemEntry.Companion.getTABLE_NAME(), id, name, cost));
        }

        cursor.close();
        database.close();
        return receiptItems;
    }
}
