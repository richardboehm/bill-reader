package me.richardboehm.ocrreader.util

import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.utils.ViewPortHandler

import java.text.NumberFormat

/**
 * Helper class to format a float into a currency format. Currently takes the currency information
 * from the phones language and region setting. In future versions the user should be able to switch
 * to the desired currency or maybe recognize the currency on the bill.
 */
class CurrencyValueFormatter : IValueFormatter {

    private val format: NumberFormat

    init {
        format = NumberFormat.getCurrencyInstance()
    }

    override fun getFormattedValue(value: Float, entry: Entry, dataSetIndex: Int, viewPortHandler: ViewPortHandler): String {
        return format.format(value.toDouble())
    }
}
