package me.richardboehm.ocrreader.models

import android.provider.BaseColumns

/**
 * This class represents a receipt item, also called article. It contains the name, the cost,
 * category and the reference to the receipt. The class also implements BaseColumns in
 * the subclass @[ReceiptItem.ReceiptItemEntry] to be used as a table in the
 * SQLite database @[me.richardboehm.ocrreader.util.DatabaseHelper].
 *
 * @author Richard Böhm
 * @version 1.0
 */
class ReceiptItem {

    var id: Long? = null
    var name: String
    var cost: Float = 0.toFloat()
    var category: Int = 0
    val receiptId: Long

    /**
     * Used to create a @[ReceiptItem] from a backup/import.
     * @param id Id of the item.
     * @param name Name of the item.
     * @param cost Cost of the item.
     * @param category Category of the item.
     * @param receiptId ID of the receipt this item belongs to.
     */
    constructor(id: Long, name: String, cost: Float, category: Int, receiptId: Long) {
        this.id = id
        this.name = name
        this.cost = cost
        this.category = category
        this.receiptId = receiptId
    }

    /**
     * Constructor for creating a new @[ReceiptItem]
     * @param name Name of the item.
     * @param cost Cost of the item.
     * @param category Category of the item.
     * @param receiptId ID of the receipt this item belongs to.
     */
    constructor(name: String, cost: Float, category: Int, receiptId: Long) {
        this.name = name
        this.cost = cost
        this.category = category
        this.receiptId = receiptId
    }

    class ReceiptItemEntry : BaseColumns {
        companion object : KBaseColumns() {
            val TABLE_NAME = "ReceiptItem"
            val COLUMN_NAME_NAME = "Name"
            val COLUMN_NAME_COST = "Cost"
            val COLUMN_NAME_CATEGORY = "Category"
            val COLUMN_NAME_RECEIPT = "ReceiptID"
        }
    }

    /**
     * Prettifies the output for the debug console.
     * @return Returns a prettified string representation of this class.
     */
    override fun toString(): String {
        return "ID: " + id + "\t Name: " + name + "\t Cost: " + cost + "\t Category" + Category.getCategory(category) + "\t Receipt: " + receiptId
    }
}
