package me.richardboehm.ocrreader.models

import android.provider.BaseColumns
import me.richardboehm.ocrreader.models.Receipt.ReceiptEntry

/**
 * This class represents a receipt. It contains the DateTime and the total sum of costs. The class
 * also implements BaseColumns in the subclass @[ReceiptEntry] to be used as a table in the
 * SQLite database @[me.richardboehm.ocrreader.util.DatabaseHelper].
 */
class Receipt(val id: Long, val datetime: Long?, val total: Float) {

    class ReceiptEntry : BaseColumns {
        companion object : KBaseColumns() {
            val TABLE_NAME = "Receipt"
            val COLUMN_NAME_DATETIME = "DateTime"
            val COLUMN_NAME_TOTAL = "TotalCost"
        }
    }
}
