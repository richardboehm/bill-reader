package me.richardboehm.ocrreader.models

import java.util.*

/**
 * This class represents the categories of an article. The predefined categories can be accessed
 * from any context as they are defined as static fields. It is also possible to retrieve the
 * integer representation of a category by it's name.
 *
 * @author Richard Böhm
 * @version 1.0
 */
object Category {

    val UNDEFINED = 0
    val FOOD = 1
    val TECHNOLOGY = 2
    val UTILITY = 3


    val categories = arrayOf("Undefined", "Food", "Technology", "Utility")

    /**
     * Returns the string representation of a given integer. The integer represents a category
     * defined in this class.
     * @param i Identifier of the category.
     * @return Returns the string representation of the category.
     */
    fun getCategory(i: Int): String {
        return categories[i]
    }

    /**
     * Returns the identifier of a given string. The string represents a category defined in this
     * class.
     * @param i String representation of the category.
     * @return Returns the identifier of the category.
     */
    fun getCategory(i: String): Int {
        val index = Arrays.binarySearch(categories, i)
        return if (index > 0) index else 0
    }
}
